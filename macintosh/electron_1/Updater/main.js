/*

Author: Sean Oberoi
Organization: Pirates Online Retribution
Product: Pirates Online Retribution Launcher
Date: 3/10/16
Copyright 2016 Sean Oberoi and Pirates Online Retribution

*/
const electron = require('electron');
const remote = electron.remote;
const app = electron.app
const shelly = electron.shell;
const BrowserWindow = electron.BrowserWindow
const os = require('os');
const dllwd = require('electron-dl');
const asyncx = require('async');
const nativeImage = require('electron').nativeImage
const shellLocal = require('shelljs');
const md5File = require('md5-file')

var request = require('request');
let imagee = nativeImage.createFromPath('./resources/images/pirates.ico')
let mainWindow
var newWin = null;


function createWindow() {
  mainWindow = new BrowserWindow({
    name: "Pirates Online Retribution Updater",
    width: 511,
    height: 323,
    transparent: true,
    /* Allows for minimize window */
    frame: false,
    /* Allows for framed window */
    toolbar: false,
    /* Disallows toolbar from being visible on top of launcher window */
    resize: false,
    /* Reassures for no resize */
    movable: true,
    /* Reassures for movable window */
    minimizable: true,
    /* Allows for minimize window */
    closable: true,
    /* Allows for quit window */
    devTools: true /* This should be false for production, but kept true for testings */
  })
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  mainWindow.setResizable(false)
  mainWindow.setMovable(true)
  //mainWindow.openDevTools();
  //mainWindow.isMovable()
  mainWindow.on('closed', function() {
    mainWindow = null
  })
  
}

app.on('ready', function() {
  createWindow()
})

app.dock.setIcon(imagee)
app.dock.show()
app.on('window-all-closed', function() {
  app.quit()
})
app.on('activate', function() {
  if (mainWindow === null) {
    createWindow()
  }
})

function closeWindow() {
  var windowee = remote.getCurrentWindow();
  windowee.close();
}

function minWindow() {
  var windowe = remote.getCurrentWindow();
  windowe.minimize();
}

function openLink(url) {
  shelly.openExternal(url);
}

function getFilenameFromUrl(url) {
  return url.substring(url.lastIndexOf('/') + 1);
}

function getBeforeUrl(url) {
  return url.substr(0, url.lastIndexOf('/'));
}
var download_file_httpget = function(file_url) {
  var options = {
    host: url.parse(file_url).host,
    port: 80,
    path: url.parse(file_url).pathname
  };
}

function checkLauncher(urlPath, zipHash) {
 
  
  var fs = require('fs')
  var url = require('url');
  var http = require('http');
  var exec = require('child_process').exec;
  var spawn = require('child_process').spawn;
  var homePath = os.homedir();

  var launcherURL = "https://raw.githubusercontent.com/POROperations/mac-releases/master/" + urlPath;
  var launcherName = getFilenameFromUrl(launcherURL);
  var launcherPath = homePath + '/PORLauncher/';
  var launcherPath1 = homePath + '/PORLauncher/PORLauncher.zip';
  var asarPath = homePath + '/PORLauncher/PORLauncher.app/Contents/Resources/app.asar';
  var asarPath2 = homePath + '/PORLauncher/PORLauncher.app/Contents/Resources/';
  //var sAsarHash; // = api pointing to asar hash on github
  
  
  if (!fs.existsSync(launcherPath)) {
    console.log("PORLauncher folder doesnt exist!")
         
    var newDir = homePath + '/PORLauncher/';
    var mkdir = 'mkdir -p ' + newDir;
    var child = exec(mkdir, function(err, stdout, stderr) {
      if (err) throw err;
    });
    console.log("Folder does not exist, creating and downloading new app!")
    downloadLauncher(launcherPath1, launcherURL, launcherName);
  
  } else if (!fs.existsSync(launcherPath1)) {
    console.log("PORLauncher.zip doent exist")
    downloadLauncher(launcherPath1, launcherURL, launcherName);
    
  } else if (fs.existsSync(launcherPath1)) {
    var fileSizeStats = fs.statSync(launcherPath1);
    var byteFileSize = fileSizeStats["size"];
    const hash = md5File.sync(launcherPath1)
      if (hash != zipHash) {
        downloadLauncher(launcherPath1, launcherURL, launcherName);// download new asar and replace - asarPath is where it will be saved
        console.log(hash + " is not equal to: " + zipHash);
      } else {
        console.log("File exists and is of correct hash!")
        
       startLauncher(); // start launcher from asarPath
      }
  }  
   
}


function downloadAsar(asarPath, asarURL) {
  var fs = require('fs')
  var received_bytes = 0;
  var total_bytes = 0;
  var req = request({
    method: 'GET',
    uri: asarURL
  });
  var out = fs.createWriteStream(asarPath);
  req.pipe(out);
  req.on('response', function(data) {
    total_bytes = parseInt(data.headers['content-length']);
  });
  req.on('data', function(chunk) {
    received_bytes += chunk.length;
    //document.getElementById("accountBox").innerHTML = "<div id='loadBarIng'></div>";
  });
  req.on('end', function() {
    console.log("File succesfully downloaded");
      //unZip(asarPath);
  });
}

function downloadLauncher(launcherPath, launcherURL, launcherName) {
  var fs = require('fs');
  var received_bytes = 0;
  var total_bytes = 0;
  var totale = 0;
  var len = 1;
  var req = request({
    method: 'GET',
    uri: launcherURL
  });
  var out = fs.createWriteStream(launcherPath);
  req.pipe(out);
  req.on('response', function(data) {
    total_bytes += parseInt(data.headers['content-length']);
  });
  req.on('data', function(chunk) {
    received_bytes += chunk.length;
    var divd = 100 / (len);
    var percentage = Math.round((received_bytes * 100) / total_bytes);
    totale += divd;
    var kek = percentage * totale;
    //showProgress(received_bytes, total_bytes);
    document.getElementById("loadBarIng").className =
      "loadBar";
        document.getElementById('loadBarIng').innerHTML =
      "<progress value='" + percentage +
      "' max='100'>" + percentage + "%</progress>";
  });
  req.on('end', function() {
    console.log("File succesfully downloaded");
      unZip(launcherName);
  });
}

function showProgress(received, total) {
  var percentage = Math.round((received * 100) / total);
  console.log(percentage);
  document.getElementById("alertMessage").className =
    "alert alert-trans";
  document.getElementById('alertMessage').innerHTML =
  "<progress value='" + percentage + "' max='100'>" + percentage + "%</progress>"
    //"<strong>Updating Resources: " + percentage + "% </strong>";
 }

function unZip(launcherPath) {
  var fs = require('fs');
  console.log("ew");
  var exec = require('child_process').exec;
    exec('rm -rf ' + launcherPath, {shell: '/bin/bash'}, function(code, stdout, stderr) {
      console.log('Exit:', code);
      console.log('Output:', stdout);
      console.log('Error:', stderr);
      });
      
      var unzip = 'unzip ' + fileName;
      exec(unzip, (error, stdout, stderr) => {
        if (error) {
          console.error('Error in unzipping: ' + fileName);
          return;
        }
      });
      console.log("unzipped: " + fileName);
      setTimeout(function(){
        startLauncher();
      }, 4000);
  
   
}

function startLauncher() {
  var userHomeDir = os.homedir();
  var fs = require("fs");
  var exec = require('child_process').exec;
  var spawn = require('child_process').spawn;
  var execSync = require('child_process').execSync;

  shellLocal.cd();
  console.log(1);
  
  shellLocal.cd('PORLauncher');
  console.log(2);
  
  
  
  var currentDir = shellLocal.pwd();
  console.log(currentDir);
  
  exec('open PORLauncher.app', {shell: '/bin/bash'}, function(code, stdout, stderr) {
        console.log('Exit:', code);
        console.log('Output:', stdout);
        console.log('Error:', stderr);
        var windowee = remote.getCurrentWindow();
        windowee.close();
        
  });


}


