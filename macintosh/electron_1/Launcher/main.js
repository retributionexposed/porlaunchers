/*

    Author: Sean Oberoi
    Organization: Pirates Online Retribution
    Product: Pirates Online Retribution Launcher
    Date: 3/10/16
    Copyright 2016 Sean Oberoi and Pirates Online Retribution

*/

const electron = require('electron');
const remote = electron.remote;
const app = electron.app
const shelly = electron.shell;
const BrowserWindow = electron.BrowserWindow
const os = require('os');
const dllwd = require('electron-dl');
const asyncx = require('async');
const nativeImage = require('electron').nativeImage
const shellLocal = require('shelljs');
const md5File = require('md5-file')
var request = require('request');
let imagee = nativeImage.createFromPath('./resources/images/pirates.ico')
let mainWindow
var newWin = null;

function closeWindow() {
    var windowe = remote.getCurrentWindow();
    windowe.close();
}

function minWindow() {
    var windowe = remote.getCurrentWindow();
    windowe.minimize();
}

function reloadWindow() {
    var windowe = remote.getCurrentWindow();
    windowe.reload();
}

function openLink(url) {
    shelly.openExternal(url);
}

function getFilenameFromUrl(url) {
    return url.substring(url.lastIndexOf('/') + 1);
}

function getBeforeUrl(url) {
    return url.substr(0, url.lastIndexOf('/'));
}
var download_file_httpget = function(file_url) {
    var options = {
        host: url.parse(file_url).host,
        port: 80,
        path: url.parse(file_url).pathname
    };
}

function checkForPatch(homePath, loginDataParse, resourceParse) {
    var data = resourceParse;
    var dataLength = resourceParse.length;
    var fieldName = Object.keys(data);
    var fieldLength = fieldName.length;
    var fs = require("fs");
    var url = require('url');
    var http = require('http');
    var exec = require('child_process').exec;
    var spawn = require('child_process').spawn;
    var itemsQueue = [];
    for (var k = 0; k < fieldLength; k++) {
        var resourcePath = fieldName[k];
        var fileSize = data[resourcePath];
        console.log(resourcePath + " - " + fileSize);
        var resourcePathUnGZ = resourcePath.replace(".gz", "");
        var finishPath = homePath + '/PiratesOnlineRetribution/' + resourcePathUnGZ;
        var shouldPath = homePath + '/PiratesOnlineRetribution/' + resourcePath;
        var fileURL = "https://raw.githubusercontent.com/POROperations/releases/master/" + resourcePath;
        var filename = getFilenameFromUrl(fileURL);
        var piratesOnlineRetributionFolder = homePath + '/PiratesOnlineRetribution/';
        var beforeURL = getBeforeUrl(resourcePath);
        var finalPath = piratesOnlineRetributionFolder + resourcePath;
        if (fs.existsSync(finishPath)) {
            var fileSizeStats = fs.statSync(finishPath);
            var byteSizeFile = fileSizeStats["size"];
            if (resourcePath == "porgame.dll") {
                const hash = md5File.sync(shouldPath)
                console.log(`The MD5 hash of resourcePath is: ${hash}`)
                if (hash != fileSize) {
                    itemsQueue.push(resourcePath);
                    console.log(hash + " is not equal to: " + fileSize);
                } else {
                    console.log("File exists and is of correct size!")
                }
            } else if (resourcePath == "Pirates") {
                const hash = md5File.sync(shouldPath)
                console.log(`The MD5 hash of resourcePath is: ${hash}`)
                if (hash != fileSize) {
                    itemsQueue.push(resourcePath);
                    console.log(hash + " is not equal to: " + fileSize);
                } else {
                    console.log("File exists and is of correct size!")
                }
            } else if (byteSizeFile == fileSize) {
                console.log("File exists and is of correct size!")
            } else if (byteSizeFile != fileSize) {
                itemsQueue.push(resourcePath);
                console.log("File exists but is not of correct size, downloading now!")
            }
        } else {
            var DOWNLOAD_DIR = homePath + '/PiratesOnlineRetribution/resources/default/';
            var mkdir = 'mkdir -p ' + DOWNLOAD_DIR;
            var child = exec(mkdir, function(err, stdout, stderr) {
                if (err) throw err;
            });
            itemsQueue.push(resourcePath);
        }
    }
    console.log(itemsQueue);
    if (itemsQueue.length > 0) {
        var finishD = [];
        var len = itemsQueue.length;
        var received_bytes = 0;
        var total_bytes = 0;
        var totale = 0;
        document.getElementById("mainH").style = "-webkit-app-region: drag; background-image: url(./resources/images/window_background_patching.png);";
        document.getElementById("accountBox").innerHTML = "<div id='loadBarIng'></div><label id='percentNum' class='percentNum'></label>";
        for (var i = 0; i < len; i++) {
            var resourcePath2 = itemsQueue[i];
            console.log(itemsQueue);
            var fileURL2 = "https://raw.githubusercontent.com/POROperations/releases/master/" + resourcePath2;
            var filename2 = getFilenameFromUrl(fileURL2);
            var piratesOnlineRetributionFolder2 = homePath + '/PiratesOnlineRetribution/';
            var beforeURL2 = getBeforeUrl(resourcePath2);
            var finalPath2 = piratesOnlineRetributionFolder2 + resourcePath2;
            var kek = finalPath2;
            console.log(finalPath2);
            var req = request({
                method: 'GET',
                uri: fileURL2
            });
            var out = fs.createWriteStream(finalPath2);
            req.pipe(out);
            req.on('response', function(data) {
                total_bytes += parseInt(data.headers['content-length']);
            });
            req.on('data', function(chunk) {
                received_bytes += chunk.length;
                var divd = 100 / (len);
                var percentage = Math.round((received_bytes * 100) / total_bytes);
                totale += divd;
                var kek = percentage * totale;
                document.getElementById("loadBarIng").className = "loadBar";
                document.getElementById('loadBarIng').innerHTML = "<progress value='" + percentage + "' max='100'>" + percentage + "%</progress>";
                document.getElementById('percentNum').innerHTML = "Progress: " + percentage + "%";
                console.log(percentage);
            });
            req.on('end', function() {
                console.log(finalPath2 + " succesfully downloaded");
                if (resourcePath2.includes("Pirates")) {
                    shellLocal.chmod('+x', './Pirates');
                    console.log("kek"); 
                }
                var pth;
                var pth2;
                for (var t = 0; t < len; t++) {
                    pth = itemsQueue[t];
                    pth2 = piratesOnlineRetributionFolder2 + pth;
                    if (pth.includes(".mf.gz")) {
                        gunzipSrc(pth2);
                    }
                    console.log("pth2 " + pth2);
                }  
                finishD.push(resourcePath2);
                console.log(finishD);
                console.log(finishD.length);
                console.log("----");
                console.log(itemsQueue);
                console.log(itemsQueue.length);
                if (finishD.length == itemsQueue.length) {
                    checkForLaunch(loginDataParse, itemsQueue);
                } else {
                    console.log("not ready yet!");
                }
            });
        }
    } else {
        console.log("Ready for Launch!");
        getDependencies(loginDataParse);
    }
}

function gunzipSrc(path) {
    var exec = require('child_process').exec;
    shellLocal.cd();
    console.log(path);
    var gunzip = 'gzip -d ' + path;
    exec(gunzip, (error, stdout, stderr) => {
        if (error) {
            console.error("couldnt " + gunzip);
            return;
        }
    });
    return;
}

function checkForLaunch(loginDataParse, itemsQueue) {
    getDependencies(loginDataParse);
}

function showProgress(received, total) {
    var recv = 0;
    var ttl = 0;
    recv += received;
    ttl += total;
    var percentage = Math.round((received * 100) / total);
    if (percentage < 100) {
        document.getElementById("alertMessage").className = "alert alert-trans";
        document.getElementById('alertMessage').innerHTML = "<progress value='" + percentage + "' max='100'>" + percentage + "%</progress>"
    } else if (percentage == 100) {
        document.getElementById("alertMessage").className = "alert alert-info";
        document.getElementById('alertMessage').innerHTML = "<strong>Please wait 5 seconds, press play again if the game doesnt start!</strong>";
    }
}

function createWindow() {
    mainWindow = new BrowserWindow({
        name: "PORLauncher",
        width: 806,
        height: 561,
        transparent: true,
        frame: false,
        toolbar: false,
        resize: false,
        movable: true,
        minimizable: true,
        closable: true,
        devTools: false
    })
    mainWindow.loadURL(`file://${__dirname}/index.html`)
    mainWindow.setResizable(false)
    mainWindow.setMovable(true) 
    mainWindow.on('closed', function() {
        mainWindow = null
    })
}

function getUserPath(loginDataParse, resourceParse) {
    var userHome = os.homedir();
    checkForPatch(userHome, loginDataParse, resourceParse)
}
app.on('ready', function() {
    createWindow()
})
app.dock.setIcon(imagee)
app.dock.show()
app.on('window-all-closed', function() {
    app.quit()
})
app.on('activate', function() {
    if (mainWindow === null) {
        createWindow()
    }
})

function startGame(dataParsed, resourceParse) {
    var homePath = os.homedir();
    var fs = require("fs");
    var url = require('url');
    var http = require('http');
    var exec = require('child_process').exec;
    var spawn = require('child_process').spawn;
    var playCookie = dataParsed.token;
    var sIP = dataParsed.ip;
    var data = resourceParse;
    var dataLength = resourceParse.length;
    var fieldName = Object.keys(data);
    var fieldLength = fieldName.length;
    var fs = require("fs");
    var url = require('url');
    var http = require('http');
    var itemsQueue = [];
    for (var k = 0; k < fieldLength; k++) {
        var resourcePath = fieldName[k];
        var fileSize = data[resourcePath];
        console.log(resourcePath + " - " + fileSize);
        var finishPath = homePath + '/' + resourcePath;
        var shouldPath = homePath + '/' + resourcePath;
        var fileURL = "https://raw.githubusercontent.com/POROperations/mac-releases/master/" + resourcePath;
        var filename = getFilenameFromUrl(fileURL);
        var piratesOnlineRetributionFolder = homePath + '/PiratesOnlineRetribution/';
        var beforeURL = getBeforeUrl(resourcePath);
        var finalPath = piratesOnlineRetributionFolder + resourcePath;
        if (!fs.existsSync(shouldPath)) {
            itemsQueue.push(resourcePath);
        } else if (fs.existsSync(shouldPath)) {
            const hash = md5File.sync(shouldPath)
            console.log(`The MD5 hash of resourcePath is: ${hash}`)
            if (hash != fileSize) {
                itemsQueue.push(resourcePath);
                console.log(hash + " is not equal to: " + fileSize);
            } else {
                console.log("File exists and is of correct size!")
            }
        }
    }
    console.log(itemsQueue);
    if (itemsQueue.length > 0) {
        var finishD = [];
        var len = itemsQueue.length;
        var received_bytes = 0;
        var total_bytes = 0;
        var totale = 0;
        document.getElementById("mainH").style = "-webkit-app-region: drag; background-image: url(./resources/images/window_background_patching.png);";
        document.getElementById("accountBox").innerHTML = "<div id='loadBarIng'></div><label id='percentNum' class='percentNum'></label>";
        for (var i = 0; i < len; i++) {
            var resourcePath2 = itemsQueue[i];
            console.log(itemsQueue);
            var fileURL2 = "https://raw.githubusercontent.com/POROperations/mac-releases/master/" + resourcePath2;
            var filename2 = getFilenameFromUrl(fileURL2);
            var finalPath2 = homePath + '/' + resourcePath2;
            var kek = finalPath2;
            console.log(finalPath2);
            var req = request({
                method: 'GET',
                uri: fileURL2
            });
            var out = fs.createWriteStream(finalPath2);
            req.pipe(out);
            req.on('response', function(data) {
                total_bytes += parseInt(data.headers['content-length']);
            });
            req.on('data', function(chunk) {
                received_bytes += chunk.length;
                var divd = 100 / (len);
                var percentage = Math.round((received_bytes * 100) / total_bytes);
                totale += divd;
                var kek = percentage * totale;
                document.getElementById("loadBarIng").className = "loadBar";
                document.getElementById('loadBarIng').innerHTML = "<progress value='" + percentage + "' max='100'>" + percentage + "%</progress>";
                document.getElementById('percentNum').innerHTML = "Progress: " + percentage + "%";
                console.log(percentage);
            });
            req.on('end', function() {
                console.log(finalPath2 + " succesfully downloaded");
                var pth;
                var pth2;
                for (var t = 0; t < len; t++) {
                    pth = itemsQueue[t];
                    pth2 = homePath + '/' + pth;
                    if (pth.includes(".zip")) {
                        unZip(pth2);
                    }
                    console.log("pth2 " + pth2);
                }
                finishD.push(resourcePath2);
                console.log(finishD);
                console.log(finishD.length);
                console.log("----");
                console.log(itemsQueue);
                console.log(itemsQueue.length);
                if (finishD.length >= itemsQueue.length) {
                    console.log("Lib Path Present!");
                    console.log("Frameworks Path Present!");
                    shellLocal.cd();
                    console.log(1);
                    shellLocal.cd('PiratesOnlineRetribution');
                    console.log(2);
                    shellLocal.env['POR_PLAYCOOKIE'] = playCookie;
                    console.log(shellLocal.env['POR_PLAYCOOKIE']);
                    shellLocal.env['POR_GAMESERVER'] = sIP;
                    console.log(shellLocal.env['POR_GAMESERVER']);
                    var current = shellLocal.pwd();
                    console.log(current);
                    
                    shellLocal.chmod('+x', './Pirates');
                    exec('./Pirates', {silent:true}).stdout;
                    console.log("done1");
                    makeAccountBox();
                } else {
                    console.log("not ready yet!");
                }
            });
        }
    } else {
        console.log("Lib Path Present!");
        console.log("Frameworks Path Present!");
        shellLocal.cd();
        console.log(1);
        shellLocal.cd('PiratesOnlineRetribution');
        console.log(2);
        shellLocal.env['POR_PLAYCOOKIE'] = playCookie;
        console.log(shellLocal.env['POR_PLAYCOOKIE']);
        shellLocal.env['POR_GAMESERVER'] = sIP;
        console.log(shellLocal.env['POR_GAMESERVER']);
        var current = shellLocal.pwd();
        console.log(current);
        
        shellLocal.chmod('+x', './Pirates');
        exec('./Pirates', {silent:true}).stdout;
        console.log("done2");
        makeAccountBox();
        
    }
}

function unZip(path) {
    var exec = require('child_process').exec;
    shellLocal.cd();
    console.log(path);
    var unzip = 'unzip ' + path;
    exec(unzip, (error, stdout, stderr) => {
        if (error) {
            console.error('Error in unzipping: ' + fileName);
            return;
        }
    });
    return;
}

function makeAccountBox() {
    
    document.getElementById("mainH").style = "-webkit-app-region: drag; background-image: url(./resources/images/window_background.png);";
    
    var createLink = String(onclick="openLink('http://piratesonline.us/register')");
    var createButtonClass = String("createButton");
    
    var mgLink = String(onclick="openLink('http://piratesonline.us/account')");
    var mgButtonClass = String("manageB");
    
    var frgLink = String(onclick="openLink('http://piratesonline.us/login')");
    var frgButtonClass = String("forgotButton");
    
    var plyLink = String(onclick="checkInfo()");
    var plyButtonClass = String("playButton");

    var  alertContainer = "<div id='alertMessage'></div><div id='container'></div>";
    
    var unmInp = "<input id='username-box' name='user' class='input usernameInput'></input>";
    
    var pwdInp = "<input id='password-box' name='pass' class='input passwordInput' type='password'></input>";
    
    var plyButton = "<button id='play-button' onclick=" + plyLink + " class=" + plyButtonClass + "></button>";

    var frgButton = "<button id='forgot-button' onclick=" + frgLink + " class=" + frgButtonClass + "></button>";
    
    var mgButton = "<button id='manageB-button' onclick=" + mgLink + " class=" + mgButtonClass + "></button>";
    
    var cButton = "<button id='create-button' onclick=" + createLink + " class=" + createButtonClass + "></button>";
    
    document.getElementById("accountBox").innerHTML = alertContainer + unmInp + pwdInp + plyButton + frgButton + mgButton + cButton;

}