package com.por.launcher;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JOptionPane;

import org.json.JSONObject;

public class LoginThread extends LauncherThread {

	private String failedResponse = null;
	private String playCookie;
	private String gameServer;
	private int code;
	protected String username;
	protected String password;
	
	public LoginThread(Main main, String username, String password) {
		super(main);
		this.username = username;
		this.password = password;
	}
	
	public String getFailedResponse() {
		return failedResponse;
	}
	
	public String getPlayCookie() {
		return playCookie;
	}
	
	public String getGameServer() {
		return gameServer;
	}
	
	public int getCode() {
		return code;
	}
	
	@Override
	protected Void doInBackground() {
		JSONObject json;
		
		try {
		    URLConnection connection = new URL("https://piratesonline.us/_t.php").openConnection();
		    
		    connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		    
			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
	
		    writer.write(String.format("user=%s&pass=%s", username, password));
		    writer.flush();
	
		    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		    String line = reader.readLine();

		    json = new JSONObject(line);
		} catch (Exception e) {
			failedResponse = "Couldn't establish connection with game server!";
			return null;
		}
		    
	    if (json.getString("status").equals("error") || !json.has("token")) {
	    	failedResponse = json.getString("message");
	    	return null;
	    }
	    
	    playCookie = json.get("token").toString();
	    gameServer = json.get("ip").toString();
	    
	    getMain().frame.setVisible(false);

	    try {
			code = getMain().runGame(playCookie, gameServer);
		} catch (Exception e) {
			code = -1;
		}

	    return null;
	}
	
	@Override
	public void done() {
		String failedMessage = getFailedResponse();
		
		if (failedMessage != null) {
			JOptionPane.showMessageDialog(null, "We couldn't log you in, because:\n\n" + failedMessage, "Launcher", JOptionPane.ERROR_MESSAGE);
			getMain().frame.setVisible(true);
			getMain().setButtonState(true);
			return;
		}
		
		getMain().frame.setVisible(true);
		getMain().setButtonState(true);
		getMain().passwordField.setText("");

		if (code != 0) {
			int option = JOptionPane.showConfirmDialog(null, "Your game has crashed. Would you like to send in a bug report?", "Launcher", JOptionPane.YES_NO_OPTION);
			
			if (option == JOptionPane.YES_OPTION) {
				Utils.openBrowser("http://piratesonline.us/contact");
			}
		}
	}
}