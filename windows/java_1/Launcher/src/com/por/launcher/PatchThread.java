package com.por.launcher;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

import org.json.JSONObject;

public class PatchThread extends LauncherThread {

	private String failedResponse = null;
	
	public PatchThread(Main main) {
		super(main);
	}
	
	public String getFailedResponse() {
		return failedResponse;
	}
	
	@Override
	protected Void doInBackground() {
		String os = OSValidator.getOS();
		
		if (os != "win" && os != "mac") {
			failedResponse = "Your operating system is unsupported! Try playing with Windows or Mac!";
			return null;
		}
		
		String baseLink = "https://raw.githubusercontent.com/POROperations/releases/master";
		String link = String.format("%s/patchmanifest_%s_dev.json", baseLink, os);
		String manifestText = Utils.readLink(link);
		
		if (manifestText == null) {
			failedResponse = "Sorry, but I couldn't download the patch manifest! Maybe you're offline?";
			return null;
		}

		JSONObject json = new JSONObject(manifestText);
		Set<String> keys = json.keySet();
		
		if (keys.isEmpty()) {
			failedResponse = "Sorry, but I couldn't parse the patch manifest!";
			return null;
		}
		
		Set<String> redownload = new HashSet<String>();
		
		for (String key : json.keySet()) {
			String filename = key.replace(".gz", "");
			File file = new File(filename);
			
			if (file.getParentFile() != null) {
				file.getParentFile().mkdirs();
			}
			
			if (!file.exists() || file.isDirectory()) {
				redownload.add(key);
			} else {
				Object expectedValue = json.get(key);
				
				if (expectedValue instanceof Integer) {
					if (file.length() != (int) expectedValue) {
						redownload.add(key);
					}
				} else if (expectedValue instanceof String) {
					try {
						if (!MD5Checksum.getMD5Checksum(filename).equals(json.getString(filename))) {
							redownload.add(key);
						}
					} catch (Exception e) {
						failedResponse = String.format("Couldn't check %s!", filename);
						return null;
					}
				}
			}
		}
		
		Set<String> gzipped = new HashSet<String>();
		
		for (String key : redownload) {
			link = String.format("%s/%s", baseLink, key);
			
			try {
				Utils.downloadFile(link, key);
				
				if (key.contains(".gz")) {
					Utils.gunzipFile(key);
					gzipped.add(key);
				}
				
			} catch (Exception e) {
				failedResponse = String.format("Couldn't download %s!", key);
				return null;
			}
		}
		
		try {
			Thread.sleep(250);
			
			for (String key : gzipped) {
				Files.delete(Paths.get(key));
			}
		} catch (Exception e) {
			// Not an important failure.
		}

		return null;
	}
	
	@Override
	public void done() {
		String failedMessage = getFailedResponse();

		if (failedMessage != null) {
			JOptionPane.showMessageDialog(null, failedMessage + "\n\nThe game couldn't be patched. Quitting now...", "Launcher", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
			return;
		}

		getMain().patchDone();
	}
}