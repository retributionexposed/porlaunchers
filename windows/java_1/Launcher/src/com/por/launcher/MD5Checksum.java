package com.por.launcher;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class MD5Checksum {

   public static byte[] createChecksum(String filename) throws Exception {
       InputStream input =  new FileInputStream(filename);

       byte[] buffer = new byte[1024];
       MessageDigest complete = MessageDigest.getInstance("MD5");
       int numRead;

       do {
           numRead = input.read(buffer);
           if (numRead > 0) {
               complete.update(buffer, 0, numRead);
           }
       } while (numRead != -1);

       input.close();
       return complete.digest();
   }

   public static String getMD5Checksum(String filename) throws Exception {
       byte[] bytes = createChecksum(filename);
       String result = "";

       for (int i=0; i < bytes.length; i++) {
           result += Integer.toString( ( bytes[i] & 0xff ) + 0x100, 16).substring( 1 );
       }

       return result;
   }
}