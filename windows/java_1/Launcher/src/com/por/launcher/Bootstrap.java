package com.por.launcher;

import javax.swing.UIManager;

public class Bootstrap {

	public static void main(String[] args) {
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// Not important.
		}

		new Main();
	}	
}