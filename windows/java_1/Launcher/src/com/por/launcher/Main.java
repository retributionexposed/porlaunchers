package com.por.launcher;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.KeyListener;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import javafx.scene.input.KeyEvent;

public class Main
{
	
	public String OS;
	public JFrame frame;
	
	public Icon closeIcon1;
	public Icon closeIcon2;
	public Icon minIcon1;
	public Icon minIcon2;
	
	public int closeLocationX;
	public int closeLocationY;
	public int minLocationX;
	public int minLocationY;
	
	public URL soundclip;

	public JButton playButton;
	public JTextField loginField;
	public JPasswordField passwordField;
	public JLabel patchText;
	public JLabel accountManageImage;
	
	public JButton forgotButton;
	public JButton createButton;
	
	public Main()
	{
		// Get the OS that the launcher is running on
		OS = System.getProperty("os.name").toLowerCase();
		
		// Creating the main frame
		frame = new JFrame("Pirates Online Retribution - Launcher");
        frame.setUndecorated(true);
        frame.setResizable(false);
		
		//Load Click Sound
		final AudioClip click = Applet.newAudioClip(Main.class.getResource("/Audio/SNDCLICK.wav"));
		
		//Setting the icon
        ImageIcon icon = new ImageIcon(Main.class.getResource("/Icon/pirates.png"));
        frame.setIconImage(icon.getImage());
        
        JPanel panel = new MotionPanel(frame);
        panel.setLayout(new FlowLayout());
        
        //Making the window transparent
        panel.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        frame.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        
        JLabel background = new JLabel(new ImageIcon(Main.class.getResource("/Backgrounds/FinalBackground.png")));
        
		//Creating minimize and exit buttons
        if (OS.indexOf("win") >= 0 || OS.indexOf("nux") >= 0)
		{
            closeIcon1 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Windows-Linux/TQUIT1U.jpg"));
            closeIcon2 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Windows-Linux/TQUIT1D.jpg"));
            minIcon1 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Windows-Linux/TMIN1U.jpg"));
            minIcon2 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Windows-Linux/TMIN1D.jpg"));
            closeLocationX = 769;
            closeLocationY = 22;
            minLocationX = 752;
            minLocationY = 22;
		}
		else if (OS.indexOf("mac") >= 0)
		{
            closeIcon1 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Mac/window_close_button_normal.png"));
            closeIcon2 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Mac/window_close_button_hover.png"));
            minIcon1 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Mac/window_minimize_button_normal.png"));
            minIcon2 = new ImageIcon(Main.class.getResource("/Buttons/WindowManage/Mac/window_minimize_button_hover.png"));
            closeLocationX = 762;
            closeLocationY = 26;
            minLocationX = 737;
            minLocationY = 26;
		}
        final JButton closeButton = new JButton(closeIcon1);
        closeButton.setBorderPainted(false); 
        closeButton.setContentAreaFilled(false); 
        closeButton.setFocusPainted(false); 
        closeButton.setOpaque(false);
        closeButton.setSize(20, 20);
        closeButton.setLocation(closeLocationX, closeLocationY);
        closeButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	closeButton.setIcon(closeIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
                closeButton.setIcon(closeIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	System.exit(0);
            }
        });
        final JButton minButton = new JButton(minIcon1);
        minButton.setBorderPainted(false); 
        minButton.setContentAreaFilled(false); 
        minButton.setFocusPainted(false); 
        minButton.setOpaque(false);
        minButton.setSize(20, 20);
        minButton.setLocation(minLocationX, minLocationY);
        minButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	minButton.setIcon(minIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
                minButton.setIcon(minIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	frame.setState(Frame.ICONIFIED);
            }
        });
        
        //Creating the login & password image
        patchText = new JLabel("Updating...");
        patchText.setForeground(new Color(255, 0, 0));
        patchText.setFont(new Font(patchText.getFont().getName(), Font.PLAIN, 15));
        patchText.setSize(135, 70);
        patchText.setLocation(555, 180);
        JLabel accountManageImage = new JLabel(new ImageIcon(Main.class.getResource("/Components/LIPROMPT.jpg")));
        accountManageImage.setSize(101, 80);
        accountManageImage.setLocation(420, 215);
        
        //Creating the login field
        loginField = new JTextField(20);
        loginField.setSize(125, 20);
        loginField.setLocation(525, 227);
        loginField.addKeyListener(new KeyListener()
        {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				if(e.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
				{
					startLogIn(loginField.getText(), String.valueOf(passwordField.getPassword()));
				}
			}

			@Override
			public void keyReleased(java.awt.event.KeyEvent e)
			{
				return;
			}

			@Override
			public void keyTyped(java.awt.event.KeyEvent e)
			{
				return;
			}
        });
        
        //Creating the password field
        passwordField = new JPasswordField(20);
        passwordField.setSize(125, 20);
        passwordField.setLocation(525, 249);
        passwordField.addKeyListener(new KeyListener()
        {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				if(e.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
				{
					startLogIn(loginField.getText(), String.valueOf(passwordField.getPassword()));
				}
			}

			@Override
			public void keyReleased(java.awt.event.KeyEvent e)
			{
				return;
			}

			@Override
			public void keyTyped(java.awt.event.KeyEvent e)
			{
				return;
			}
        });
        
        //Creating the play button
        final Icon playIcon1 = new ImageIcon(Main.class.getResource("/Buttons/PlayButton/PLAY1D.jpg"));
        final Icon playIcon2 = new ImageIcon(Main.class.getResource("/Buttons/PlayButton/PLAY1R.jpg"));
        final Icon playIcon3 = new ImageIcon(Main.class.getResource("/Buttons/PlayButton/PLAY1U.jpg"));
        playButton = new JButton(playIcon1);
        playButton.setBorderPainted(false); 
        playButton.setContentAreaFilled(false); 
        playButton.setFocusPainted(false); 
        playButton.setOpaque(false);
        playButton.setSize(82, 30);
        playButton.setLocation(661, 229);
        playButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	playButton.setIcon(playIcon2);
            	
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	playButton.setIcon(playIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	playButton.setIcon(playIcon3);
            	click.play();
            	startLogIn(loginField.getText(), String.valueOf(passwordField.getPassword()));
            }
        });
        
        //Creating the forgot password button
        final Icon forgotIcon1 = new ImageIcon(Main.class.getResource("/Buttons/ForgotPassword/FORGOT1U.jpg"));
        final Icon forgotIcon2 = new ImageIcon(Main.class.getResource("/Buttons/ForgotPassword/FORGOT1D.jpg"));
        forgotButton = new JButton(forgotIcon1);
        forgotButton.setBorderPainted(false); 
        forgotButton.setContentAreaFilled(false); 
        forgotButton.setFocusPainted(false); 
        forgotButton.setOpaque(false);
        forgotButton.setSize(103, 17);
        forgotButton.setLocation(525, 278);
        forgotButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	forgotButton.setIcon(forgotIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	forgotButton.setIcon(forgotIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/login");
            }
        });
        
        //Creating the create account button
        final Icon createIcon1 = new ImageIcon(Main.class.getResource("/Buttons/CreateAnAccount/CREATEPW1U.jpg"));
        final Icon createIcon2 = new ImageIcon(Main.class.getResource("/Buttons/CreateAnAccount/CREATEPW1D.jpg"));
        createButton = new JButton(createIcon1);
        createButton.setBorderPainted(false); 
        createButton.setContentAreaFilled(false); 
        createButton.setFocusPainted(false); 
        createButton.setOpaque(false);
        createButton.setSize(103, 17);
        createButton.setLocation(630, 278);
        createButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	createButton.setIcon(createIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	createButton.setIcon(createIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/register");
            }
        });
        
        //Adding all the footer buttons
        //Creating the official site button
        final Icon officialIcon1 = new ImageIcon(Main.class.getResource("/Buttons/OfficialSite/WEBSITE1D.jpg"));
        final Icon officialIcon2 = new ImageIcon(Main.class.getResource("/Buttons/OfficialSite/WEBSITE1R.jpg"));
        final JButton FOOTERofficialButton = new JButton(officialIcon1);
        FOOTERofficialButton.setBorderPainted(false); 
        FOOTERofficialButton.setContentAreaFilled(false); 
        FOOTERofficialButton.setFocusPainted(false); 
        FOOTERofficialButton.setOpaque(false);
        FOOTERofficialButton.setSize(117, 23);
        FOOTERofficialButton.setLocation(35, 510);
        FOOTERofficialButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERofficialButton.setIcon(officialIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERofficialButton.setIcon(officialIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/");
            }
        });
        
        //Creating the community site button
        final Icon communityIcon1 = new ImageIcon(Main.class.getResource("/Buttons/Community/COMMUNITY1D.jpg"));
        final Icon communityIcon2 = new ImageIcon(Main.class.getResource("/Buttons/Community/COMMUNITY1R.jpg"));
        final JButton FOOTERcommunityButton = new JButton(communityIcon1);
        FOOTERcommunityButton.setBorderPainted(false); 
        FOOTERcommunityButton.setContentAreaFilled(false); 
        FOOTERcommunityButton.setFocusPainted(false); 
        FOOTERcommunityButton.setOpaque(false);
        FOOTERcommunityButton.setSize(117, 23);
        FOOTERcommunityButton.setLocation(158, 510);
        FOOTERcommunityButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERcommunityButton.setIcon(communityIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERcommunityButton.setIcon(communityIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("https://piratesforums.com");
            }
        });
        
        //Creating the players guide button
        final Icon playerguideIcon1 = new ImageIcon(Main.class.getResource("/Buttons/PlayersGuide/FAQ1D.jpg"));
        final Icon playerguideIcon2 = new ImageIcon(Main.class.getResource("/Buttons/PlayersGuide/FAQ1R.jpg"));
        final JButton FOOTERplayerguideButton = new JButton(playerguideIcon1);
        FOOTERplayerguideButton.setBorderPainted(false); 
        FOOTERplayerguideButton.setContentAreaFilled(false); 
        FOOTERplayerguideButton.setFocusPainted(false); 
        FOOTERplayerguideButton.setOpaque(false);
        FOOTERplayerguideButton.setSize(117, 23);
        FOOTERplayerguideButton.setLocation(281, 510);
        FOOTERplayerguideButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERplayerguideButton.setIcon(playerguideIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERplayerguideButton.setIcon(playerguideIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesforums.us/faq");
            }
        });
        
        //Creating the players guide button
        final Icon releasenotesIcon1 = new ImageIcon(Main.class.getResource("/Buttons/ReleaseNotes/HOMEPAGE1D.jpg"));
        final Icon releasenotesIcon2 = new ImageIcon(Main.class.getResource("/Buttons/ReleaseNotes/HOMEPAGE1R.jpg"));
        final JButton FOOTERreleasenotesButton = new JButton(releasenotesIcon1);
        FOOTERreleasenotesButton.setBorderPainted(false); 
        FOOTERreleasenotesButton.setContentAreaFilled(false); 
        FOOTERreleasenotesButton.setFocusPainted(false); 
        FOOTERreleasenotesButton.setOpaque(false);
        FOOTERreleasenotesButton.setSize(117, 23);
        FOOTERreleasenotesButton.setLocation(404, 510);
        FOOTERreleasenotesButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERreleasenotesButton.setIcon(releasenotesIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERreleasenotesButton.setIcon(releasenotesIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/news");
            }
        });
        
        //Creating the manage account button
        final Icon manageaccountIcon1 = new ImageIcon(Main.class.getResource("/Buttons/ManageAccount/MANAGEACCOUNT1D.jpg"));
        final Icon manageaccountIcon2 = new ImageIcon(Main.class.getResource("/Buttons/ManageAccount/MANAGEACCOUNT1R.jpg"));
        final JButton FOOTERmanageaccountButton = new JButton(manageaccountIcon1);
        FOOTERmanageaccountButton.setBorderPainted(false);
        FOOTERmanageaccountButton.setContentAreaFilled(false);
        FOOTERmanageaccountButton.setFocusPainted(false);
        FOOTERmanageaccountButton.setOpaque(false);
        FOOTERmanageaccountButton.setSize(117, 23);
        FOOTERmanageaccountButton.setLocation(527, 510);
        FOOTERmanageaccountButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERmanageaccountButton.setIcon(manageaccountIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERmanageaccountButton.setIcon(manageaccountIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/account");
            }
        });
        
        //Creating the help button
        final Icon helpIcon1 = new ImageIcon(Main.class.getResource("/Buttons/Help/REPORTBUG1D.jpg"));
        final Icon helpIcon2 = new ImageIcon(Main.class.getResource("/Buttons/Help/REPORTBUG1R.jpg"));
        final JButton FOOTERhelpButton = new JButton(helpIcon1);
        FOOTERhelpButton.setBorderPainted(false);
        FOOTERhelpButton.setContentAreaFilled(false);
        FOOTERhelpButton.setFocusPainted(false);
        FOOTERhelpButton.setOpaque(false);
        FOOTERhelpButton.setSize(117, 23);
        FOOTERhelpButton.setLocation(650, 510);
        FOOTERhelpButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt)
            {
            	FOOTERhelpButton.setIcon(helpIcon2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt)
            {
            	FOOTERhelpButton.setIcon(helpIcon1);
            }
            
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
            	click.play();
            	Utils.openBrowser("http://piratesonline.us/contact");
            }
        });
        
        //Resizing and adding components to frame...
        panel.add(background);
        frame.add(patchText);
        frame.add(accountManageImage);
        frame.add(loginField);
        frame.add(passwordField);
        frame.add(playButton);
        frame.add(forgotButton);
        frame.add(createButton);
        frame.add(closeButton);
        frame.add(minButton);
        // Start Footer Buttons
        frame.add(FOOTERofficialButton);
        frame.add(FOOTERcommunityButton);
        frame.add(FOOTERplayerguideButton);
        frame.add(FOOTERreleasenotesButton);
        frame.add(FOOTERmanageaccountButton);
        frame.add(FOOTERhelpButton);
        // End Footer Buttons
        //End Testing Stuff
        frame.add(panel);
        frame.setSize(810, 570);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        startPatching();
	}
	
	public void setButtonState(boolean enabled) {
		playButton.setEnabled(enabled);
		
		if (enabled) {
			patchText.setText("Log in!");
			patchText.setLocation(565, 180);
		}
	}
	
	public void startPatching() {
		patchText.setText("Updating...");
		patchText.setLocation(555, 180);
		setButtonState(false);
		new PatchThread(this).execute();
	}
	
	public void startLogIn(String username, String password) {
		patchText.setText("Logging in...");
		patchText.setLocation(550, 180);
		setButtonState(false);
		new LoginThread(this, username, password).execute();
	}
	
	public void patchDone() {
		setButtonState(true);
	}

	public int runGame(String playCookie, String gameServer) throws Exception {	
		String path = OSValidator.isWindows() ? "cmd /C start porgame.exe" : "./Pirates";
		Process process = Runtime.getRuntime().exec(path, new String[] {"POR_PLAYCOOKIE=" + playCookie, "POR_GAMESERVER=" + gameServer});
		return process.waitFor();
	}

}
