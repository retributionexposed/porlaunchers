﻿Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports Newtonsoft.Json
Public Class Update
    Private Const baseURL As String = "https://raw.githubusercontent.com/POROperations/releases/master/"
    Private Sub Update_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UpdateFiles()
        Try
            Process.Start("PORLauncher")
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try

        Me.Close()
    End Sub
    Public Function CompareMD5(ByVal filePath As String, ByVal hash As String)
        Dim md As String
        Try
            Using _md5 = MD5.Create()
                Using stream = File.OpenRead(filePath)
                    md = BitConverter.ToString(_md5.ComputeHash(stream)).Replace("-", "").ToLower()
                End Using
            End Using
        Catch ex As Exception
            Return False
        End Try
        If hash = md Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function IsProcessRunning(name As String) As Boolean
        For Each clsProcess As Process In Process.GetProcesses()
            If clsProcess.ProcessName.StartsWith(name) Then
                Return True
            End If
        Next
        Return False
    End Function
    Sub UpdateFiles()
        Try
            Dim request As HttpWebRequest = WebRequest.Create("https://raw.githubusercontent.com/POROperations/releases/master/launcher.json")
            request.Method = "GET"
            Dim response As WebResponse = request.GetResponse()
            Dim dataStream As Stream
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()
            Dim status As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(responseFromServer)
            Dim files As String() = New String(status.Count - 1) {}
            Dim hashes As String() = New String(status.Count - 1) {}
            status.Values.CopyTo(hashes, 0)
            status.Keys.CopyTo(files, 0)
            Dim Client As New WebClient
            For i = 0 To status.Count - 1
                If CompareMD5(files(i), hashes(i)) = True Then
                    Console.WriteLine("{0} is up to date!", files(i))
                Else
                    Console.WriteLine("{0} is not up to date. Downloading...", files(i))
                    Client.DownloadFile(baseURL & files(i), files(i))
                    Console.WriteLine("Successfully updated {0}", files(i))
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), "Error")
        End Try
        Console.WriteLine("Updating Finished.")
    End Sub
End Class
