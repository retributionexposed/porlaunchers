﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Public Class Play
    Public Shared Sub LaunchGame(ip As String, token As String)
        Environment.SetEnvironmentVariable("POR_PLAYCOOKIE", token)
        Environment.SetEnvironmentVariable("POR_GAMESERVER", ip)
        Dim startInfo As New ProcessStartInfo()
        startInfo.FileName = "porgame"
        Process.Start(startInfo)
    End Sub
    Public Shared Sub getToken(ByVal Username As String, ByVal Password As String)
        ' Requesting JSON Response & Encoding Username and Password for URL
        Dim request As WebRequest = WebRequest.Create("https://piratesonline.us/_t.php")
        request.Method = "POST"
        Dim postData As String = "user=" + Username + "&pass=" + Password
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = byteArray.Length
        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As WebResponse = request.GetResponse()
        dataStream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        Dim status As PORServerResponse = JsonConvert.DeserializeObject(Of PORServerResponse)(responseFromServer)
        Dim result As String = status.code
        Dim ip As String = status.ip
        Dim cookie As String = status.token
        Dim message As String = status.message
        If result = "0" Then
            ' Success
            Launcher.btnPlay.BackgroundImage = My.Resources.PLAY1U
            Launcher.txtPass.ForeColor = Color.Green
            Launcher.txtUser.ForeColor = Color.Green
            LaunchGame(ip, cookie)
        ElseIf result = "1" Then
            ' Account is not enabled or wrong username/password
            Launcher.btnPlay.BackgroundImage = My.Resources.PLAY1U
            Launcher.txtUser.ForeColor = Color.Red
            Launcher.txtPass.ForeColor = Color.Red
            ' Prevents ui glitch
            Launcher.txtUser.Focus()
        Else
            Launcher.btnPlay.BackgroundImage = My.Resources.PLAY1U
            Launcher.txtUser.ForeColor = Color.Red
            Launcher.txtPass.ForeColor = Color.Red
            ' Prevents ui glitch
            Launcher.txtUser.Focus()
        End If
    End Sub
End Class